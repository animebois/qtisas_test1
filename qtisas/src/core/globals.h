/******************************************************************************
Project: QtiSAS
License: GNU GPL Version 3 (see LICENSE)
Copyright (C) by the authors:
    2006 Ion Vasilief <ion_vasilief@yahoo.fr>
    2023 Konstantin Kholostov <k.kholostov@fz-juelich.de>
Description: QtiSAS version
 ******************************************************************************/

// Major version number
const int maj_version = 0;
// Minor version number
const int min_version = 9;
// Patch version number
const int patch_version = 14;
