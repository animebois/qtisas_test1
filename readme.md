# QtiSAS

QtiSAS is an analysis software for SAS (small-angle scattering) measurements
based on QtiPlot.

# Installing

One can download qtisas package with or without python scripting support. Please
refer [releases page](https://iffgit.fz-juelich.de/qtisas/qtisas/-/releases).

The latest packages are:

 * For macos arm:
   * [qtisas](https://iffgit.fz-juelich.de/api/v4/projects/1655/packages/generic/qtisas/v0.9.14/qtisas-v0.9.14-arm.dmg)
   * [qtisas-py](https://iffgit.fz-juelich.de/api/v4/projects/1655/packages/generic/qtisas/v0.9.14/qtisas-py-v0.9.14-arm.dmg)
 * For macos intel:
   * [qtisas](https://iffgit.fz-juelich.de/api/v4/projects/1655/packages/generic/qtisas/v0.9.14/qtisas-v0.9.14-intel.dmg)
   * [qtisas-py](https://iffgit.fz-juelich.de/api/v4/projects/1655/packages/generic/qtisas/v0.9.14/qtisas-py-v0.9.14-intel.dmg)
 * For windows 11 x86-32:
   * [qtisas](https://iffgit.fz-juelich.de/api/v4/projects/1655/packages/generic/qtisas/v0.9.14/qtisas-v0.9.14-x86_32.exe)
   * [qtisas-py](https://iffgit.fz-juelich.de/api/v4/projects/1655/packages/generic/qtisas/v0.9.14/qtisas-py-v0.9.14-x86_32.exe)

# Building

Refer to [building instructions](build.md).